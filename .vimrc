set nocompatible

" Show current command in statusbar
set showcmd

" Enable mouse support
set mouse=a

" Show line numbers
" (actually just shows current line, because of line below)
set number

" Show relative line numbers
set relativenumber

filetype on
filetype plugin on
filetype plugin indent on
syntax enable
syntax on

set grepprg=grep\ -nH\ $*

" Make indentation 4 characters, also expand tabs to spaces
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Automatically indent when going to next line
set autoindent

" Ignore case when searching, highlight search results
set ignorecase
set smartcase
set hlsearch

" Because my terminals are always with a black background
set background=dark

" Remap jj to quit insert mode
inoremap jj <Esc>

" Enable syntax highlighting for nginx files
au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft== '' | setfiletype nginx | endif
